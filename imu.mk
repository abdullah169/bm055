NAME := imu_Application

$(NAME)_SOURCES := imu.c \
                   BN055.c \
                   quaternion.c
                   
$(NAME)_INCLUDES   := .

VALID_PLATFORMS += RB_DUO \
                   RB_DUO_ext
