/*
    Inertial Measurement Unit Maths Library
    Copyright (C) 2013-2014  Samuel Cowen
    www.camelsoftware.com

    Bug fixes and cleanups by Gé Vissers (gvissers@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    C Port : Hüseyin Kozan <posta@huseyinkozan.com.tr>
*/


#include <math.h>


struct Quaternion {
    double w;
    double x;
    double y;
    double z;
};

typedef struct Quaternion Quaternion;


Quaternion quatFromDefault() {
    Quaternion q;
    q.w = 1.0;
    q.x = 0.0;
    q.y = 0.0;
    q.z = 0.0;
    return q;
}

Quaternion quatFromArgs(double w, double x, double y, double z) {
    Quaternion q;
    q.w = w;
    q.x = x;
    q.y = y;
    q.z = z;
    return q;
}

Quaternion quatFromQuat(Quaternion q_) {
    Quaternion q;
    q.w = q_.w;
    q.x = q_.x;
    q.y = q_.y;
    q.z = q_.z;
    return q;
}

Quaternion quatFromQuatP(Quaternion * q_) {
    Quaternion q;
    q.w = q_->w;
    q.x = q_->x;
    q.y = q_->y;
    q.z = q_->z;
    return q;
}


Quaternion quatAdd(Quaternion q1, Quaternion q2)
{
    return quatFromArgs(q1.w + q2.w, q1.x + q2.x, q1.y + q2.y, q1.z + q2.z);
}

Quaternion quatSubstruct(Quaternion q1, Quaternion q2)
{
    return quatFromArgs(q1.w - q2.w, q1.x - q2.x, q1.y - q2.y, q1.z - q2.z);
}

Quaternion quatDivide(Quaternion q, double scalar)
{
    return quatFromArgs(q.w / scalar, q.x / scalar, q.y / scalar, q.z / scalar);
}

Quaternion quatScale(Quaternion q, double scalar)
{
    return quatFromArgs(
                q.w * scalar, q.x * scalar, q.y * scalar, q.z * scalar);
}

Quaternion quatMultiply(Quaternion q1, Quaternion q2)
{
    return quatFromArgs(
        q1.w*q2.w - q1.x*q2.x - q1.y*q2.y - q1.z*q2.z,
        q1.w*q2.x + q1.x*q2.w + q1.y*q2.z - q1.z*q2.y,
        q1.w*q2.y - q1.x*q2.z + q1.y*q2.w + q1.z*q2.x,
        q1.w*q2.z + q1.x*q2.y - q1.y*q2.x + q1.z*q2.w
    );
}




double quatMagnitude(Quaternion q)
{
    return sqrt( q.w*q.w + q.x*q.x + q.y*q.y + q.z*q.z );
}

Quaternion quatConjugate(Quaternion q)
{
    return quatFromArgs(q.w, -q.x, -q.y, -q.z);
}


Quaternion quatNormalize(Quaternion q)
{
    double mag = quatMagnitude(q);
    return quatScale(q, 1 / mag );
}

Quaternion quatFromAxisAngle(
        Quaternion q, double x, double y, double z, double theta)
{
    q.w = cos(theta/2);
    //only need to calculate sine of half theta once
    double sht = sin(theta/2);
    q.x = x * sht;
    q.y = y * sht;
    q.z = z * sht;
    return q;
}

void quatToAxisAngle(
        Quaternion q, double * x, double * y, double * z, double * angle)
{
    double sqw = sqrt(1-q.w*q.w);
    if (sqw == 0) //it's a singularity and divide by zero, avoid
        return;

    *angle = 2 * acos(q.w);
    *x = q.x / sqw;
    *y = q.y / sqw;
    *z = q.z / sqw;
}

void quatToEuler(Quaternion q, double * x, double * y, double * z)
{
    double sqw = q.w*q.w;
    double sqx = q.x*q.x;
    double sqy = q.y*q.y;
    double sqz = q.z*q.z;

    *x = atan2(2.0*(q.x*q.y+q.z*q.w),(sqx-sqy-sqz+sqw));
    *y = asin(-2.0*(q.x*q.z-q.y*q.w)/(sqx+sqy+sqz+sqw));
    *z = atan2(2.0*(q.y*q.z+q.x*q.w),(-sqx-sqy+sqz+sqw));
}

void quatToAngularVelocity(
        Quaternion q, double * x, double * y, double * z, double dt)
{
    Quaternion one = quatFromArgs(1.0, 0.0, 0.0, 0.0);
    Quaternion delta = quatSubstruct(one, q);
    Quaternion r = quatDivide(delta, dt);
    r = quatScale(r, 2);
    r = quatMultiply(r, one);

    *x = r.x;
    *y = r.y;
    *z = r.z;
}


