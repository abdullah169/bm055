/*
 * imu.c
 *
 *  Created on: 27 Kas 2017
 *      Author: abdullah
 */


/*
 * Copyright 2015, RedBear Corporation
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of RedBear Corporation;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of RedBear Corporation.
 */

/** @file
 *
 * I2C Application
 *
 * This application demonstrates how to use the WICED I2C API
 * to operate Grove OLED display 0.96"
 *
 * Features demonstrated
 *  - I2C API
 *
 */

#include "wiced.h"
#include "BN055.h"



void application_start( )
{
	int8_t rx;
	/* Initialise the WICED device */
	wiced_init();
	begin(0);


	while(1){

		wiced_rtos_delay_milliseconds(500);
		rx = read8(0x34);
		WPRINT_APP_INFO( ( "temp: %d\n",rx ) );

	}

}


